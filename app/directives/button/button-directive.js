/**
 * Created by Ruslana Chelnokova on 02.08.16.
 */
(function() {
    'use strict';

    angular.module('button.directive', [])
        .directive('refreshButton', refreshButton);

    function refreshButton() {
        var directive = {
            restrict: 'E',
            templateUrl: 'directives/button/button-directive.html',
            controller: function () {

            }
        };
        return directive;
    }
}());