/**
 * Created by Ruslana Chelnokova on 02.08.16.
 */
(function(){
    'use strict';

    angular
        .module('repo.module')
        .config(repoPageRoute);

    repoPageRoute.$inject = ['$stateProvider'];

    function repoPageRoute($stateProvider) {
        $stateProvider
            .state('base.repoPage', {
                url: "/repo/:owner/:repo/:branch",
                views: {
                    content: {
                        templateUrl: 'repo-page/repo-page.html',
                        controller: 'repoPageCtrl',
                        controllerAs: 'repo',
                        resolve: {
                            commits: function ($q, $stateParams, GithubService) {
                                var defer = $q.defer();
                                GithubService.getCommits($stateParams).then(function (commits) {
                                    defer.resolve(commits);
                                }, function (err) {
                                    defer.reject(err);
                                });
                                return defer.promise;
                            }
                        }
                    }
                }
            })
    }

})();