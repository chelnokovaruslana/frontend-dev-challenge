/**
 * Created by Ruslana Chelnokova on 02.08.16.
 */
(function(){
    'use strict';

    angular
        .module('repo.module')
        .controller('repoPageCtrl', repoPageCtrl);

    repoPageCtrl.$inject = ['$stateParams', 'commits'];

    function repoPageCtrl($stateParams, commits) {
        var repo = this;

        repo.commits = commits;
        repo.branch = $stateParams.branch;

        repo.refreshPage = refreshPage;
        repo.dateParse = dateParse;

        ///////////////////
        function refreshPage() {
            location.reload();
        }

        function dateParse(dateString) {

            var date = new Date(dateString),
                day = date.getDate(),
                month = date.getMonth(),
                year = date.getFullYear(),
                hours = date.getHours(),
                minutes = date.getMinutes();
            return  day + '-' +month + '-' + year+' at '+ hours+':'+ minutes;
        }
    }
})();