/**
 * Created by Ruslana Chelnokova on 02.08.16.
 */
(function(){
    'use strict';

    angular.module('repo.module', [
        'bw.paging',
        'ngTagsInput'
    ]);
})();