(function(){
    'use strict';

    angular.module('welcome.module', [
        'search.module',
        'repo.module'
    ]);
})();