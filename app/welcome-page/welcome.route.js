(function(){
    'use strict';

    angular
        .module('welcome.module')
        .config(welcomePageRoute);

    welcomePageRoute.$inject = ['$stateProvider'];

    function welcomePageRoute($stateProvider) {
        $stateProvider
            .state('base.welcomePage', {
                url: "/welcome",
                views: {
                    content: {
                        templateUrl: 'welcome-page/welcome.html',
                        controller: 'welcomeCtrl',
                        controllerAs: 'welcome'
                    }
                }
            })
    }

})();