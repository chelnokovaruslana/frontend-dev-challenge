angular.module('testApp', [
    'ui.router',
    'templates',
    'ngResource',
    //-- directives ---
    'button.directive',
    //-- services ---
    'github',
    //---

    'base.view'
])
    .config(function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/welcome");
    });