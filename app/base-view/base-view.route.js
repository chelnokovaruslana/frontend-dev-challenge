(function(){
    'use strict';

    angular
        .module('base.view')
        .config(baseViewRoute);

    baseViewRoute.$inject = ['$stateProvider'];

    function baseViewRoute($stateProvider) {
        $stateProvider
            .state('base', {
                url: "",
                templateUrl: 'base-view/base-view.html',
                controller: 'baseCtrl',
                controllerAs: 'base',
                abstract: true
            })
    }
})();
