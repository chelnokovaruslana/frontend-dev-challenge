(function(){
    'use strict';

    angular.module('search.module', [
        'bw.paging',
        'ngTagsInput'
    ]);
})();