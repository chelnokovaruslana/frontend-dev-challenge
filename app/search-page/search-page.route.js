(function(){
    'use strict';

    angular
        .module('search.module')
        .config(searchPageRoute);

    searchPageRoute.$inject = ['$stateProvider'];

    function searchPageRoute($stateProvider) {
        $stateProvider
            .state('base.searchPage', {
                url: "/search",
                views: {
                    content: {
                        templateUrl: 'search-page/search-page.html',
                        controller: 'searchPageCtrl',
                        controllerAs: 'search'
                    }
                }
            })
    }

})();