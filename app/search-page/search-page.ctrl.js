(function(){
    'use strict';

    angular
        .module('search.module')
        .controller('searchPageCtrl', searchPageCtrl);

    searchPageCtrl.$inject = ['$state', 'GithubService'];

    function searchPageCtrl($state, GithubService) {
        var search = this;
        //default search
        search.tags = [
            {'text': 'angular'},
            {'text': 'angularjs'}
        ];
        search.repoes = [];
        search.pagination = {};
        search.error = {};

        search.searchBy = searchBy;
        search.dateParse = dateParse;
        search.paginationSearch = paginationSearch;
        search.clearArray = clearArray;
        search.goToRepo = goToRepo;


        ////////////////////////////////////
        function searchBy(tags) {
            var searchTags = tags.map(function(tag) {
                return tag.text;
            });
            GithubService.search({
                searchBy: searchTags
            }).then(function(results) {
                search.error.show = false;
                search.repoes = results.items;
                search.pagination.total = Math.floor(results.total_count / 30);
                search.pagination.page = 1;
            }, function() {
                search.error.show = true;
            });
        }

        function dateParse(dateString) {
           var date = new Date(dateString),
               day = date.getDate(),
               month = date.getMonth(),
               year = date.getFullYear();
            return  month + '-' + day + '-' + year;
        }

        function paginationSearch(tags, page) {
            clearArray(search.repoes);
            var searchTags = tags.map(function(tag) {
                return tag.text;
            });
            GithubService.search({
                searchBy: searchTags,
                page: page
            }).then(function(results) {
                search.error.show = false;
                search.repoes = results.items;
                search.pagination.page = page;
            }, function() {
                search.error.show = true;
            });
        }

        function goToRepo(repo) {
            $state.go('base.repoPage', { owner: repo.owner.login, repo: repo.name, branch: 'master' });
        }

        function clearArray(array) {
            array.length = 0;
            search.error.show = false;
        }
    }
})();