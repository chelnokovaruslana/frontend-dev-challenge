(function(){
    'use strict';

    angular.module('github')
        .factory('GithubSearchResource', GithubSearchResource)
        .factory('GithubCommitsResource', GithubCommitsResource)
        .service('GithubService', GithubService);

    GithubSearchResource.$inject = ['$resource', 'API_CONFIG'];
    GithubCommitsResource.$inject = ['$resource', 'API_CONFIG'];
    GithubService.$inject = ['$q', 'GithubSearchResource', 'GithubCommitsResource'];


    function GithubCommitsResource($resource, API_CONFIG) {
        return $resource(API_CONFIG.URL+'/repos/:owner/:repo/commits', {
            sha: '@sha'
        },
        {
            'get': {
                method:'GET',
                isArray: true
            }
        })
    }

    function GithubSearchResource($resource, API_CONFIG) {
        return $resource(API_CONFIG.URL+'/search/repositories', {
            q: '@q',
            page: '@page'
        });
    }

    function GithubService($q, GithubSearchResource, GithubCommitsResource) {
        this.search = function(params) {
            var defer = $q.defer(),
                searchBy = params.searchBy,
                page = params.page ? params.page : 0;
            GithubSearchResource.get(null, {q: searchBy, page: page}).$promise.then(function(result){
                defer.resolve(result);
            }, function(err){
                defer.reject();
            });
            return defer.promise;
        };

        this.getCommits = function(params) {
            var defer = $q.defer(),
                owner = params.owner,
                repo = params.repo,
                branch = params.branch;
            GithubCommitsResource.get({ owner: owner, repo: repo }, { sha: branch }).$promise.then(function(result){
                defer.resolve(result);
            }, function(err) {
                defer.reject();
            });
            return defer.promise;
        }
    }

})();